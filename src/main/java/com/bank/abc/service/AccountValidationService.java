package com.bank.abc.service;

import com.bank.abc.model.Account;
import com.bank.abc.model.AccountStatus;
import com.bank.abc.model.ResponseObject;

public class AccountValidationService {

    public ResponseObject validateAccount(Account account){
        ResponseObject responseObject = new ResponseObject();
        if(account.getAccountNo() < 0){
            responseObject.getErrors().add("account number is not valid");
        }
        if(!account.getAccountStatus().equals(AccountStatus.ACTIVE)){
            responseObject.getErrors().add("Account status "+account.getAccountStatus()+" is not valid for transaction");
        }
        if(responseObject.getErrors().size()==0){
            responseObject.setMessage("All validation passed, Account is valid");
        }
        return responseObject;
    }
}
