package com.bank.abc.service;

import com.bank.abc.model.*;

import java.util.Date;

public class TransactionValidationService {

    public ResponseObject validateTransaction(Customer customer, Transaction transaction){
        ResponseObject responseObject = new ResponseObject();
        switch (transaction.getTransactionType()){
            case DEBIT:
                responseObject = validateDebitTransaction(customer,transaction);

        }
        return responseObject;
    }

    private ResponseObject validateDebitTransaction(Customer customer, Transaction transaction){
        ResponseObject responseObject = new ResponseObject();
        if(!transaction.getSourceAccountNumber().equals(customer.getAccountInfo().getAccountNo())){
            responseObject.getErrors().add("source account number and transaction account number does not match");
        }
        if(transaction.getTransactionAmount() > customer.getAccountInfo().getBalance()){
            responseObject.getErrors().add("transaction amount is more than the balance in account");
        }
        if(customer.getAccountInfo().getWithdrawLimitPerDay() > (totalTransactionAmountForTheDay(customer.getAccountInfo())+transaction.getTransactionAmount())){
            responseObject.getErrors().add("Transaction amount is more than daily withdraw limit");
        }
        if(transaction.getTransactionAmount() > customer.getAccountInfo().getWithdrawLimitPerPersonPerDay()){
            responseObject.getErrors().add("transaction amount is more than the daily withdraw limit per person");
        }

        if(responseObject.getErrors().size() ==0 ){
            responseObject.setMessage("All validation passed, Transaction is valid");
        }
        return responseObject;
    }

    private Double totalTransactionAmountForTheDay(Account account){
        double sum = 0.0;
        for(Transaction transaction : account.getTransactionList()){
            if(transaction.getTransactionStatus().equals(TransactionStatus.COMPLETED) && (new Date().getTime() == transaction.getEndTime().getTime())){
                sum+=transaction.getTransactionAmount();
            }
        }
        return sum;
    }

}
