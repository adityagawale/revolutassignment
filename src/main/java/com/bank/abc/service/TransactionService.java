package com.bank.abc.service;

import com.bank.abc.model.*;

public class TransactionService {
    public Account accountInfoAfterTransaction(Account account, Transaction transaction, TransactionStatus transactionStatus){
        if(transactionStatus.equals(TransactionStatus.COMPLETED)){
            account.setCurrentTransactionActive(false);
            account.setBalance(account.getBalance() - transaction.getTransactionAmount());
            account.setInProgressTransactionAmount(0.0);
        }else if(transactionStatus.equals(TransactionStatus.FAILED)){
            account.setCurrentTransactionActive(false);
            account.setInProgressTransactionAmount(0.0);
        }
        /*save account info in db*/
        return account;
    }
    public Account accountInfoBeforeTransaction(Account account, Transaction transaction){
        transaction.setTransactionStatus(TransactionStatus.IN_PROGRESS);
        account.getTransactionList().add(transaction);
        account.setCurrentTransactionActive(true);
        account.setInProgressTransactionAmount(transaction.getTransactionAmount());
        /*save account and transaction info in db*/
        return account;
    }
    public void doTransaction(Customer customer, Account account, Transaction transaction){
        /*check if any transaction is active*/
        pauseTransaction(account);
        /* validate account */
        AccountValidationService accountValidationService = new AccountValidationService();
        ResponseObject accountValidationResponse = accountValidationService.validateAccount(account);
        account = accountInfoBeforeTransaction(account,transaction);
        if(accountValidationResponse.getErrors().size() > 0){
            System.out.println("Account is not valid");
            account = accountInfoAfterTransaction(account,transaction,TransactionStatus.FAILED);
            for(String error : accountValidationResponse.getErrors()){
                System.out.println(error);
            }
            return;
        }
        /*validate transaction*/
        TransactionValidationService transactionValidationService = new TransactionValidationService();
        ResponseObject transactionValidationResposne = transactionValidationService.validateTransaction(customer,transaction);
        if(transactionValidationResposne.getErrors().size() > 0){
            System.out.println("Transaction is not valid");
            for(String error : transactionValidationResposne.getErrors()){
                System.out.println(error);
            }
            account = accountInfoAfterTransaction(account,transaction,TransactionStatus.FAILED);
            return;
        }
        account = accountInfoAfterTransaction(account,transaction,TransactionStatus.COMPLETED);
        System.out.println("Transaction completed successfully");
        return;
    }

    private void pauseTransaction(Account account){
        try {
            if(account.isCurrentTransactionActive()){
                System.out.println("Old transaction is still in progress");
                Thread.sleep(10000);
                pauseTransaction(account);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
