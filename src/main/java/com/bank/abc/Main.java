package com.bank.abc;

import com.bank.abc.helper.TransactionHelper;
import com.bank.abc.model.*;
import com.bank.abc.service.TransactionService;

import java.util.Date;

public class Main {

    public static void main(String[] args) {
        TransactionHelper transactionHelper = new TransactionHelper();
        /*Success transaction*/
        System.out.println("Starting transaction 1");
        transactionHelper.successTransaction();
        System.out.println("Completed transaction 1");

        /*Failed Transaction 1*/
        System.out.println("Starting transaction 2");
        transactionHelper.failTransaction1();
        System.out.println("Completed transaction 2");

        System.out.println("Starting transaction 3");
        transactionHelper.failTransaction2();
        System.out.println("Completed transaction 3");

        System.out.println("Starting transaction 4");
        transactionHelper.failTransaction3();
        System.out.println("Completed transaction 4");
    }
}
