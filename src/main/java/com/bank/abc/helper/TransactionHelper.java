package com.bank.abc.helper;

import com.bank.abc.model.*;
import com.bank.abc.service.TransactionService;

import java.util.Date;

public class TransactionHelper {

    public void successTransaction(){
        /*Initialize customer account info  */
        Account account = new Account(12345, AccountType.SAVING,1000000.0,10000.0,10000.0,0.0, AccountStatus.ACTIVE,false);

        /*Initializing customer info*/
        Customer customer = new Customer(12345,account,"FirstName","LastName",new Date(), new Date(),new Address(), new Address(),"ssn");
        /*Initialize and add beneficiary*/
        ThirdPartyBeneficiary thirdPartyBeneficiary = new ThirdPartyBeneficiary("FirstName","LastName",123456,TransferBankType.SAMEBANK,"bankName","branchCode");

        /*Initialize transaction object*/
        Transaction transaction = new Transaction(123456789,12345,thirdPartyBeneficiary,10000.0,TransactionType.DEBIT,new Date(),new Date(),TransactionStatus.SUBMITTED);

        TransactionService transactionService = new TransactionService();
        transactionService.doTransaction(customer,account,transaction);
    }
    public void failTransaction1(){
        /*Initialize customer account info  */
        Account account = new Account(12345, AccountType.SAVING,1000000.0,10000.0,500.0,0.0, AccountStatus.ACTIVE,false);

        /*Initializing customer info*/
        Customer customer = new Customer(12345,account,"FirstName","LastName",new Date(), new Date(),new Address(), new Address(),"ssn");
        /*Initialize and add beneficiary*/
        ThirdPartyBeneficiary thirdPartyBeneficiary = new ThirdPartyBeneficiary("FirstName","LastName",123456,TransferBankType.SAMEBANK,"bankName","branchCode");

        /*Initialize transaction object*/
        Transaction transaction = new Transaction(123456789,12345,thirdPartyBeneficiary,10000.0,TransactionType.DEBIT,new Date(),new Date(),TransactionStatus.SUBMITTED);

        TransactionService transactionService = new TransactionService();
        transactionService.doTransaction(customer,account,transaction);
    }

    public void failTransaction2(){
        /*Initialize customer account info  */
        Account account = new Account(12345, AccountType.SAVING,1000000.0,10000.0,500.0,0.0, AccountStatus.DISABLED,false);

        /*Initializing customer info*/
        Customer customer = new Customer(12345,account,"FirstName","LastName",new Date(), new Date(),new Address(), new Address(),"ssn");
        /*Initialize and add beneficiary*/
        ThirdPartyBeneficiary thirdPartyBeneficiary = new ThirdPartyBeneficiary("FirstName","LastName",123456,TransferBankType.SAMEBANK,"bankName","branchCode");

        /*Initialize transaction object*/
        Transaction transaction = new Transaction(123456789,12345,thirdPartyBeneficiary,10000.0,TransactionType.DEBIT,new Date(),new Date(),TransactionStatus.SUBMITTED);

        TransactionService transactionService = new TransactionService();
        transactionService.doTransaction(customer,account,transaction);
    }
    public void failTransaction3(){
        /*Initialize customer account info  */
        Account account = new Account(12345, AccountType.SAVING,1000000.0,10000.0,10000.0,0.0, AccountStatus.ACTIVE,false);

        /*Initializing customer info*/
        Customer customer = new Customer(12345,account,"FirstName","LastName",new Date(), new Date(),new Address(), new Address(),"ssn");
        /*Initialize and add beneficiary*/
        ThirdPartyBeneficiary thirdPartyBeneficiary = new ThirdPartyBeneficiary("FirstName","LastName",123456,TransferBankType.SAMEBANK,"bankName","branchCode");

        /*Initialize transaction object*/
        Transaction transaction = new Transaction(123456789,123456,thirdPartyBeneficiary,10000.0,TransactionType.DEBIT,new Date(),new Date(),TransactionStatus.SUBMITTED);

        TransactionService transactionService = new TransactionService();
        transactionService.doTransaction(customer,account,transaction);
    }

}
