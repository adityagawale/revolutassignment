package com.bank.abc.model;

import java.util.ArrayList;
import java.util.List;

public class ResponseObject {
    private List<String> errors = new ArrayList<>();
    private String message;

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
