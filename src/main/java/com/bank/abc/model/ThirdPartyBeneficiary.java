package com.bank.abc.model;

public class ThirdPartyBeneficiary {

    private String firstName;
    private String lastName;
    private Integer accountNo;
    private TransferBankType transferBankType;
    private String bankName;
    private String branchCode;

    public ThirdPartyBeneficiary(String firstName, String lastName, Integer accountNo, TransferBankType transferBankType, String bankName, String branchCode) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountNo = accountNo;
        this.transferBankType = transferBankType;
        this.bankName = bankName;
        this.branchCode = branchCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(Integer accountNo) {
        this.accountNo = accountNo;
    }

    public TransferBankType getTransferBankType() {
        return transferBankType;
    }

    public void setTransferBankType(TransferBankType transferBankType) {
        this.transferBankType = transferBankType;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
}
