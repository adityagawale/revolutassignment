package com.bank.abc.model;

import java.util.Date;

public class Transaction {
    private Integer referenceNumber;
    private Integer sourceAccountNumber;
    private ThirdPartyBeneficiary beneficiary;
    private double transactionAmount;
    private TransactionType transactionType;
    private Date startTime;
    private Date endTime;
    private TransactionStatus transactionStatus;

    public Transaction(Integer referenceNumber, Integer sourceAccountNumber, ThirdPartyBeneficiary beneficiary, double transactionAmount, TransactionType transactionType, Date startTime, Date endTime, TransactionStatus transactionStatus) {
        this.referenceNumber = referenceNumber;
        this.sourceAccountNumber = sourceAccountNumber;
        this.beneficiary = beneficiary;
        this.transactionAmount = transactionAmount;
        this.transactionType = transactionType;
        this.startTime = startTime;
        this.endTime = endTime;
        this.transactionStatus = transactionStatus;
    }

    public Integer getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(Integer referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public Integer getSourceAccountNumber() {
        return sourceAccountNumber;
    }

    public void setSourceAccountNumber(Integer sourceAccountNumber) {
        this.sourceAccountNumber = sourceAccountNumber;
    }

    public ThirdPartyBeneficiary getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(ThirdPartyBeneficiary beneficiary) {
        this.beneficiary = beneficiary;
    }

    public double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public TransactionStatus getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(TransactionStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }
}
