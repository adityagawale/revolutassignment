package com.bank.abc.model;

public enum AccountStatus {
    ACTIVE,
    DISABLED,
    BLOCKED,
    DELETED;
}
