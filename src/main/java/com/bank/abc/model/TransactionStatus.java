package com.bank.abc.model;

public enum TransactionStatus {
    IN_PROGRESS,
    COMPLETED,
    FAILED,
    SUBMITTED;
}
