package com.bank.abc.model;

public enum AccountType {
    SAVING,
    CURRENT;
}
