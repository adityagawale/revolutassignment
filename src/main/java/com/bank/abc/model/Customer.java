package com.bank.abc.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Customer {
    private Integer CIN;
    private Account accountInfo;
    private String firstName;
    private String lastName;
    private Date dob;
    private Date anniversary;
    private Address homeAddress;
    private Address workAddress;
    private String ssnNo;
    private List<ThirdPartyBeneficiary> thirdPartyBeneficiaryList = new ArrayList<>();

    public Customer(Integer CIN, Account accountInfo, String firstName, String lastName, Date dob, Date anniversary, Address homeAddress, Address workAddress, String ssnNo) {
        this.CIN = CIN;
        this.accountInfo = accountInfo;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
        this.anniversary = anniversary;
        this.homeAddress = homeAddress;
        this.workAddress = workAddress;
        this.ssnNo = ssnNo;
        this.thirdPartyBeneficiaryList = new ArrayList<>();
    }

    public Integer getCIN() {
        return CIN;
    }

    public void setCIN(Integer CIN) {
        this.CIN = CIN;
    }

    public Account getAccountInfo() {
        return accountInfo;
    }

    public void setAccountInfo(Account accountInfo) {
        this.accountInfo = accountInfo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Date getAnniversary() {
        return anniversary;
    }

    public void setAnniversary(Date anniversary) {
        this.anniversary = anniversary;
    }

    public Address getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(Address homeAddress) {
        this.homeAddress = homeAddress;
    }

    public Address getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(Address workAddress) {
        this.workAddress = workAddress;
    }

    public String getSsnNo() {
        return ssnNo;
    }

    public void setSsnNo(String ssnNo) {
        this.ssnNo = ssnNo;
    }

    public List<ThirdPartyBeneficiary> getThirdPartyBeneficiaryList() {
        return thirdPartyBeneficiaryList;
    }

    public void setThirdPartyBeneficiaryList(List<ThirdPartyBeneficiary> thirdPartyBeneficiaryList) {
        this.thirdPartyBeneficiaryList = thirdPartyBeneficiaryList;
    }
}
