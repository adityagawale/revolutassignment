package com.bank.abc.model;

public enum Country {
    INDIA,
    AUSTRALIA,
    ENGLAND,
    USA;
}
