package com.bank.abc.model;

public enum TransactionType {
    DEBIT,
    CREDIT;

}
