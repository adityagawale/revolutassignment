package com.bank.abc.model;

public enum TransferBankType {
    SAMEBANK,
    OTHERBANK
}
