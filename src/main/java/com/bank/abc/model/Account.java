package com.bank.abc.model;

import java.util.ArrayList;
import java.util.List;

public class Account {
    private Integer accountNo;
    private AccountType accountType;
    private double balance;
    private Double withdrawLimitPerDay;
    private Double withdrawLimitPerPersonPerDay;
    private double inProgressTransactionAmount;
    private AccountStatus accountStatus;
    private boolean isCurrentTransactionActive;
    private List<Transaction> transactionList;

    public Account(Integer accountNo, AccountType accountType, double balance, Double withdrawLimitPerDay, Double withdrawLimitPerPersonPerDay, double inProgressTransactionAmount, AccountStatus accountStatus, boolean isCurrentTransactionActive) {
        this.accountNo = accountNo;
        this.accountType = accountType;
        this.balance = balance;
        this.withdrawLimitPerDay = withdrawLimitPerDay;
        this.withdrawLimitPerPersonPerDay = withdrawLimitPerPersonPerDay;
        this.inProgressTransactionAmount = inProgressTransactionAmount;
        this.accountStatus = accountStatus;
        this.isCurrentTransactionActive = isCurrentTransactionActive;
        this.transactionList = new ArrayList<>();
    }

    public Integer getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(Integer accountNo) {
        this.accountNo = accountNo;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Double getWithdrawLimitPerDay() {
        return withdrawLimitPerDay;
    }

    public void setWithdrawLimitPerDay(Double withdrawLimitPerDay) {
        this.withdrawLimitPerDay = withdrawLimitPerDay;
    }

    public Double getWithdrawLimitPerPersonPerDay() {
        return withdrawLimitPerPersonPerDay;
    }

    public void setWithdrawLimitPerPersonPerDay(Double withdrawLimitPerPersonPerDay) {
        this.withdrawLimitPerPersonPerDay = withdrawLimitPerPersonPerDay;
    }

    public double getInProgressTransactionAmount() {
        return inProgressTransactionAmount;
    }

    public void setInProgressTransactionAmount(double inProgressTransactionAmount) {
        this.inProgressTransactionAmount = inProgressTransactionAmount;
    }

    public AccountStatus getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
    }

    public boolean isCurrentTransactionActive() {
        return isCurrentTransactionActive;
    }

    public void setCurrentTransactionActive(boolean currentTransactionActive) {
        isCurrentTransactionActive = currentTransactionActive;
    }

    public List<Transaction> getTransactionList() {
        return transactionList;
    }

    public void setTransactionList(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }
}
